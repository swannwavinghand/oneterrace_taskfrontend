export const environment = {
  production: true,

  apiRoot: 'https://task-management-oneterrace.herokuapp.com/terrace/',
  baseUrl: 'https://task-management-oneterrace.herokuapp.com/'
};
