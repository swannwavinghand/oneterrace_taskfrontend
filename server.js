const express = require('express');

const app = express();

app.use(express.static('./dist/taskmanagementclient'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/taskmanagementclient/'}),
);

app.listen(process.env.PORT || 8080);
